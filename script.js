const board = (() => {
  let gameBoard = {
    array: ["-", "-", "-", "-", "-", "-", "-", "-", "-"],
  };

  function showTile(currentValue, index, arr) {
    const element = document.getElementById("container");
    const button = document.createElement("button");

    button.textContent = currentValue;
    button.className = "button";
    element.appendChild(button);
    button.addEventListener("click", function () {
      gameLogic.changeTile(index, currentValue);
    });
  }

  function render() {
    document.getElementById("container").innerHTML = "";
    gameBoard.array.forEach(showTile);
  }

  render();

  return {
    render: render,
    gameBoard,
  };
})();

var gameLogic = (() => {
  var gameEnded = false;

  const winningAxes = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
  ];

  function changeTile(index, currentValue) {
    if (!(currentValue === "-")) {
      return;
    }

    if (gameEnded) {
      return;
    }

    if (player1.active) {
      board.gameBoard.array[index] = "x";
    } else if (player2.active) {
      board.gameBoard.array[index] = "0";
    }
    board.render();
    checkWin();
    changePlayer();
  }

  function changePlayer() {
    if (player1.active) {
      activePlayer = player2.name;
      player1.active = false;
      player2.active = true;
    } else if (player2.active) {
      activePlayer = player1.name;
      player1.active = true;
      player2.active = false;
    }
  }

  function checkWin() {
    winningAxes.forEach((item, index) => {
      if (
        board.gameBoard.array[item[0]] === board.gameBoard.array[item[1]] &&
        board.gameBoard.array[item[1]] === board.gameBoard.array[item[2]] &&
        !(board.gameBoard.array[item[0]] === "-")
      ) {
        showWinner();
        return;
      }
    });
    if (!board.gameBoard.array.includes("-")) {
      showWinner("tie");
    }
  }

  function showWinner(tie) {
    if (!gameEnded) {
      gameEnded = true;
    } else {
      return;
    }

    const newelement = document.getElementById("winner");
    const text = document.createElement("p");

    newelement.appendChild(text);

    const button = document.createElement("button");
    button.textContent = "Restart";

    newelement.appendChild(button);
    button.addEventListener("click", function () {
      reset();
    });

    if (tie) {
      text.innerText = " Tie!";
    } else {
      text.innerText = activePlayer + " wins!";
    }
  }

  function reset() {
    gameEnded = false;
    player1.active = true;
    player2.active = false;
    document.getElementById("winner").innerHTML = "";
    board.gameBoard.array = ["-", "-", "-", "-", "-", "-", "-", "-", "-"];
    board.render();
  }

  function createPlayer(name) {
    const score = 0;
    const active = false;
    return { name, score, active };
  }

  const player1 = createPlayer("player 1");
  const player2 = createPlayer("player 2");
  var activePlayer = "test";

  player1.active = true;

  return {
    changeTile: changeTile,
  };
})();
